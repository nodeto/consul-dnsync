import dataclasses
import signal
import sys
import threading
from pathlib import Path
from typing import Any, Dict, List, Tuple, TypedDict

import httpx
import yaml

from consul_dnsync.logger import get_logger

sighup_received = threading.Event()


def signal_handler(signum, _):
    """Signal handler for SIGHUP and SIGTERM."""
    if signum == signal.SIGHUP:
        print("SIGHUP received.")
        sighup_received.set()
    elif signum == signal.SIGTERM:
        print("SIGTERM received. Exiting.")
        sys.exit(0)


@dataclasses.dataclass
class Config:
    headers: dict[str, str]
    consul_file_path: Path
    url: str = "https://api.vultr.com/v2/"
    wait_for_sighup: bool = False


class CnameConsulRecord(TypedDict):
    """Define a cname consul record.

    Attributes:
    ----------
        name: The name of the record
        domain: The domain of the record
        data: The data/target dns address
    """

    name: str
    domain: str
    data: str


class TailnetConsulRecord(TypedDict):
    """Define a tailnet consul record.

    Attributes:
    ----------
        name: The name of the record
        host: The host of the record
        protocol: The protocol of the record
        port: The port of the record
    """

    name: str


class DnsRecordDict(TypedDict):
    """TypedDict for representing a DNS record.

    Attributes:
        type (str): The type of the DNS record.
        name (str): The name of the DNS record.
        ttl (int): The time-to-live of the DNS record.
        data (str): The content of the DNS record.
        priority (int): The record's priority
    """

    type: str
    name: str
    ttl: int
    data: str
    priority: int


class DnsExistingRecordDict(DnsRecordDict):
    """TypedDict for representing a DNS record. - including the id of the existing record

    Attributes:
        id (str): The unique identifier of the DNS record.
    """

    id: str


class ConsulServiceDetailsDict(TypedDict):
    """TypedDict for representing Consul service details.

    Attributes:
        name (str): The name of the service.
        address (str): The address of the service.
        tags (List[str]): The tags associated with the service.
    """

    name: str
    address: str
    tags: List[str]


class ConsulDataDict(TypedDict):
    """Represent the nomad-rendered templates structure.

    Attributes:
        services: List of service details which are used to create dns records.
        cnames: List of cname records.
    """

    services: List[ConsulServiceDetailsDict]
    cnames: List[CnameConsulRecord]
    tailnet_proxy: List[TailnetConsulRecord]


class DnsSync:
    def __init__(self, config: Config):
        self.config = config
        self.client = httpx.Client(base_url=config.url, headers=config.headers)
        self.logger = get_logger()

    def fetch_vultr_records(self, domain) -> list[DnsExistingRecordDict]:
        result = self.client.get(f"/domains/{domain}/records")
        result.raise_for_status()
        return result.json()["records"]  # type: ignore

    def get_ids_to_delete(
        self,
        existing_records: List[DnsExistingRecordDict],
        consul_records: List[DnsRecordDict],
    ) -> Tuple[List[str], List[str]]:
        """Return a list of ids from existing_records that don't match anything in consul_records.

        Args:
            existing_records: List of existing DNS records with ids.
            consul_records: List of consul DNS records without ids.

        Returns:
            List of ids from existing_records that don't match anything in consul_records.

        """
        ids_to_delete_immediately = []
        ids_to_delete_with_delay = []

        consul_names = set((x["name"] for x in consul_records))

        for record in existing_records:
            record_without_id: DnsRecordDict = {
                key: value for key, value in record.items() if key != "id"
            }  # type: ignore
            if record_without_id["name"] not in consul_names:
                ids_to_delete_with_delay.append(record["id"])
            elif record_without_id not in consul_records:
                ids_to_delete_immediately.append(record["id"])

        return ids_to_delete_immediately, ids_to_delete_with_delay

    def generate_dns_records(
        self,
        consul_services: List[ConsulServiceDetailsDict],
    ) -> Dict[str, List[DnsRecordDict]]:
        """Generate DNS records for services with specific tags.

        Args:
            consul_services: List of dictionaries with service details.

        Returns:
            the DNS records by domain
        """
        service_domains: dict[str, str] = {}

        def get_domain(consul_service: ConsulServiceDetailsDict) -> str:
            # If there is an explicit domain tag, use it.
            for tag in consul_service["tags"]:
                if tag.startswith("domain:"):
                    return tag.split(":")[1]
            # otherwise, assume nodeto.site
            return "nodeto.site"

        def get_upstream_domain(tag: str) -> str:
            for service in consul_services:
                if tag.split("-upstream")[0] in service["tags"]:
                    return get_domain(service)
            return "nodeto.site"

        for service in consul_services:
            service_domains[service["name"]] = get_domain(service)

        dns_records: dict[str, list] = {}
        # Find the domain each proxy belongs to.
        for service in consul_services:
            for tag in service["tags"]:
                if tag.endswith("-lb") or tag.startswith("dns-"):
                    a_record: DnsRecordDict = {
                        "type": "A",
                        "name": tag.lstrip("dns-"),
                        "data": service["address"],
                        "ttl": 60,
                        "priority": -1,
                    }
                    dns_records.setdefault(service_domains[service["name"]], []).append(
                        a_record
                    )

                elif tag.endswith("-upstream"):
                    cname_record: DnsRecordDict = {
                        "type": "CNAME",
                        "name": service["name"],
                        "data": f"{tag.split('-')[0]}-lb.{get_upstream_domain(tag)}",
                        "ttl": 60,
                        "priority": -1,
                    }
                    dns_records.setdefault(service_domains[service["name"]], []).append(
                        cname_record
                    )
        # This seems awkward -- Could be redone with dataclasses frozen=True
        # frozen=True makes it hashable and usable in sets.
        final_dns_records: dict[str, list[DnsRecordDict]] = {}
        for domain, domain_dns_records in dns_records.items():
            deduplicated_dns_records: list[DnsRecordDict] = []
            for domain_dns_record in domain_dns_records:
                if domain_dns_record not in deduplicated_dns_records:
                    deduplicated_dns_records.append(domain_dns_record)
            deduplicated_dns_records.sort(key=lambda x: x["type"])
            final_dns_records[domain] = deduplicated_dns_records
        return dns_records

    def get_records_to_add(
        self,
        existing_records: List[DnsExistingRecordDict],
        consul_records: List[DnsRecordDict],
    ) -> List[DnsRecordDict]:
        """Return a list of consul_records that don't match anything in existing_records.

        Args:
            existing_records: List of existing DNS records with ids.
            consul_records: List of consul DNS records without ids.

        Returns:
            List of consul_records that don't match anything in existing_records.

        """
        records_to_add = []
        existing_records_without_id = [
            {key: value for key, value in record.items() if key != "id"}
            for record in existing_records
        ]

        for record in consul_records:
            if all(
                (
                    record not in existing_records_without_id,
                    record not in records_to_add,
                )
            ):
                records_to_add.append(record)

        return records_to_add

    def delete_vultr_record(self, domain, identifier) -> None:
        self.client.delete(f"/domains/{domain}/records/{identifier}").raise_for_status()

    def create_vultr_record(self, domain, record: DnsRecordDict) -> None:
        try:
            self.client.post(
                f"/domains/{domain}/records", json=record
            ).raise_for_status()
        except httpx.HTTPStatusError as exc:
            print(record)
            print(exc.response.text)
            raise exc

    def run(self) -> None:
        self.logger.info("Loading %s...", str(self.config.consul_file_path))

        if self.config.wait_for_sighup:
            # Registering the signal handlers
            signal.signal(signal.SIGHUP, signal_handler)
            signal.signal(signal.SIGTERM, signal_handler)

        while True:
            consul_data: ConsulDataDict = yaml.safe_load(
                self.config.consul_file_path.read_text("utf-8")
            )
            consul_records_all = self.generate_dns_records(consul_data["services"])
            for cname_record in consul_data["cnames"]:
                consul_records_all.setdefault(cname_record["domain"], []).append(
                    {
                        "type": "CNAME",
                        "name": cname_record["name"],
                        "data": cname_record["data"],
                        "ttl": 60,
                        "priority": -1,
                    }
                )
            for tailnet_proxy_record in consul_data["tailnet_proxy"]:
                consul_records_all.setdefault("nodeto.site", []).append(
                    {
                        "type": "CNAME",
                        "name": tailnet_proxy_record["name"],
                        "data": "tailnet-lb.nodeto.site",
                        "ttl": 60,
                        "priority": -1,
                    }
                )
            delayed_deletes: List[Tuple[str, str]] = []
            for domain, consul_records in consul_records_all.items():
                self.logger.info("Processing domain %s...", domain)
                vultr_records_all = self.fetch_vultr_records(domain)
                vultr_records = [
                    x for x in vultr_records_all if x["type"] in ["A", "CNAME"]
                ]
                (
                    records_to_delete_immediately,
                    records_to_delete_with_delay,
                ) = self.get_ids_to_delete(vultr_records, consul_records)
                delayed_deletes.extend(
                    [(domain, x) for x in records_to_delete_with_delay]
                )
                records_to_add = self.get_records_to_add(vultr_records, consul_records)
                self.logger.info(
                    "Deleting %s records. Adding %s records.",
                    len(records_to_delete_immediately + records_to_delete_with_delay),
                    len(records_to_add),
                )

                [
                    self.delete_vultr_record(domain, x)
                    for x in records_to_delete_immediately
                ]
                [self.create_vultr_record(domain, x) for x in records_to_add]

            if self.config.wait_for_sighup:
                self.logger.info("Waiting for SIGHUP...")
                sighup_triggered = sighup_received.wait(300)
                if not sighup_triggered:
                    [
                        self.delete_vultr_record(domain=x[0], identifier=x[1])
                        for x in delayed_deletes
                    ]
                    sighup_received.wait()
                self.logger.info("SIGHUP received, updating records...")
                sighup_received.clear()
            else:
                break
