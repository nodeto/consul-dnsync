import argparse
import os
from pathlib import Path

from consul_dnsync.dnsync import Config, DnsSync


def main():
    parser = argparse.ArgumentParser(
        description="Process the path to the file containing consul services."
    )
    parser.add_argument(
        "path", type=str, help="The path to the file containing consul services."
    )
    parser.add_argument(
        "--wait-for-sighup",
        action="store_true",
        help="If set, the program will wait for SIGHUP and rerun.",
    )
    args = parser.parse_args()
    config = Config(
        {
            "Authorization": f"Bearer {os.getenv('VULTR_AUTH_TOKEN', '')}",
            "Accept": "application/json",
            "Content-Type": "application/json",
        },
        Path(args.path),
        wait_for_sighup=args.wait_for_sighup,
    )
    dns_sync = DnsSync(config)
    dns_sync.run()


if __name__ == "__main__":
    main()
