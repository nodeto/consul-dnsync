import logging
import sys
from datetime import datetime, timezone


def get_logger() -> logging.Logger:
    """Configure logging with UTC timestamps.

    This setup logs INFO and above messages to stdout and ERROR and above messages to stderr.
    """

    class InfoFilter(logging.Filter):
        def filter(self, record):
            return record.levelno <= logging.INFO

    # Create a logger
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    # Create stdout handler for INFO and DEBUG messages
    stdout_handler = logging.StreamHandler(sys.stdout)
    stdout_handler.setLevel(logging.INFO)
    stdout_handler.addFilter(InfoFilter())

    # Create stderr handler for WARNING, ERROR, and CRITICAL messages
    stderr_handler = logging.StreamHandler(sys.stderr)
    stderr_handler.setLevel(logging.WARNING)

    # Formatter with UTC timestamp
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s", "%Y-%m-%d %H:%M:%S"
    )
    formatter.converter = lambda *args: datetime.now(tz=timezone.utc).timetuple()

    # Set formatter for handlers
    stdout_handler.setFormatter(formatter)
    stderr_handler.setFormatter(formatter)

    # Add handlers to logger
    logger.addHandler(stdout_handler)
    logger.addHandler(stderr_handler)

    return logger
