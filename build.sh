#!/bin/bash

set -e
isort . && black .
docker build . -t kjoyce77/consul-dnsync:"$(poetry version -s)"
docker tag kjoyce77/consul-dnsync:"$(poetry version -s)" kjoyce77/consul-dnsync
docker push kjoyce77/consul-dnsync:"$(poetry version -s)"
docker push kjoyce77/consul-dnsync
